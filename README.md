code
==================

**Theme Name:** code

**Theme Description:** This theme houses shared assets (HTML, CSS, & JS) for CleanSlate CMS. Things like the WVU masthead, WVU footer, global stylesheets, and shared vendor assets.

**Developers name(s):** Adam Johnson & UR Web

**Dependencies necessary to work with this theme:** Global stylesheets require [Sass](http://www.sass-lang.com/) & [CodeKit](http://incident57.com/codekit/).

## How to Name Files

Please **follow the folder structure and naming convention provided by this repo**. We have tried to make it similar to people who do it right already (like the Google and jQuery CDN's).

There are instructions on how to name partials in the [layouts folder](https://stash.development.wvu.edu/projects/CST/repos/code/browse/views/layouts).

### How to pull in partials from this theme

    <r:partial name="path/to/masthead" theme="Theme Name in CleanSlate" />
    
Note that this tag automatically assumes you are in the `views` folder. You don't have to specify `views` in the path. In most cases, your tag will look something like this:

    <r:partial name="layouts/masthead--v1" theme="Code" />

### A note to designers/developers in Colleges looking for shared assets

CleanSlate allows you to create **your own** shared theme. So, if you were the Eberly college, you could create an `eberly-code`/`eberly-shared` repository and call files from it. If you are from WVUTech, you could create a `wvutech-code`/`wvutech-shared` repository... and so on.

This repository should only be edited by University Relations / Web staff.