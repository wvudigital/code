# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "stylesheets"
sass_dir = "scss"
images_dir = "images"
javascripts_dir = "javascripts"
fonts_dir = "fonts"

output_style = :compact
color_output = false

# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true

# To enable debugging comments that display the original location of your selectors, comment:
line_comments = false